package com.example.tsootoonwriteapi;

import java.io.Serializable;
import java.util.List;

public class Todos implements Serializable {

  private String id;

  private List<Todo> list;

  public String getId() {
    return this.id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public List<Todo> getList() {
    return this.list;
  }

  public void setList(List<Todo> list) {
    this.list = list;
  }

  @Override
  public String toString() {
    return "{" +
      " id='" + getId() + "'" +
      ", list='" + getList() + "'" +
      "}";
  }

}
