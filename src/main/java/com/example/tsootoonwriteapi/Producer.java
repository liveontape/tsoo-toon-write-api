package com.example.tsootoonwriteapi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class Producer {

    private static final Logger log = LoggerFactory.getLogger(Producer.class);
    private static final String TOPIC = "change_todolist_requests";

    @Autowired
    private KafkaTemplate<String, Todos> kafkaTemplate;

    public void sendMessage(Todos message) {
        log.info("#### -> Producing message -> {}", message);
        this.kafkaTemplate.send(TOPIC, message);
    }
}
