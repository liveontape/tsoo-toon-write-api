package com.example.tsootoonwriteapi;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

class KafkaProducerFactory {

  private KafkaProducerFactory() {
  }

  static KafkaProducer<String, Serializable> buildProducer() throws UnknownHostException {
    Properties config = new Properties();
    config.put("client.id", InetAddress.getLocalHost().getHostName());
    config.put("bootstrap.servers", "localhost:9092");
    config.put("acks", "all");
    config.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
    config.put("value.serializer", "org.springframework.kafka.support.serializer.JsonSerializer");
    return new KafkaProducer<>(config);
  }

  static ProducerRecord<String, Serializable> buildProducerRecord(String id, Serializable todoList) {
    return new ProducerRecord<>("change_todolist_requests", id, todoList);
  }
}
