package com.example.tsootoonwriteapi;

import java.io.Serializable;

public class Todo implements Serializable {

  private String text;

  private boolean isCompleted;

  public String getText() {
    return this.text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public boolean isIsCompleted() {
    return this.isCompleted;
  }

  public boolean getIsCompleted() {
    return this.isCompleted;
  }

  public void setIsCompleted(boolean isCompleted) {
    this.isCompleted = isCompleted;
  }

  @Override
  public String toString() {
    return "{" +
      " text='" + getText() + "'" +
      ", isCompleted='" + isIsCompleted() + "'" +
      "}";
  }

}
