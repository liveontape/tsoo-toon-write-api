package com.example.tsootoonwriteapi;

import java.io.Serializable;
import java.net.UnknownHostException;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Router {

	private static Logger log = LoggerFactory.getLogger(Router.class);

	@Autowired
	private Producer kafkaProducer;

	public static void main(String[] args) {
		SpringApplication.run(Router.class, args);
	}

	@CrossOrigin(allowedHeaders = "*", exposedHeaders = "*", maxAge = 600, origins = "http://localhost:3000", methods = {
			RequestMethod.GET })
	@GetMapping(path = "/", produces = "text/plain;charset=UTF-8")
	String up() {
		log.trace("up");
		return "up";
	}

	@CrossOrigin(allowedHeaders = "*", exposedHeaders = "*", maxAge = 600, origins = "http://localhost:3000", methods = {
			RequestMethod.PATCH })
	@PatchMapping(path = "/todos", consumes = "application/json", produces = "text/plain;charset=UTF-8")
	void patchTodos(@RequestParam(name = "id", required = true) String id, @RequestBody Todos todos) throws UnknownHostException {
		log.trace("patchTodos: id='{}', todoList='{}'", id, todos);
		kafkaProducer.sendMessage(todos);
	}

}
